package com.chat.model;

public class ChatMessage {

    private int id;
    private int user_id;
    private String message;
    private String datetime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    @SuppressWarnings("unused")
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    @SuppressWarnings("unused")
    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    @SuppressWarnings("unused")
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
