package com.chat.managers;

import android.content.Context;
import android.util.Log;
import com.chat.views.ChatMessageViewHolder;
import com.chat.R;
import com.chat.model.ChatMessage;
import com.chat.model.Contact;
import com.chat.presenters.MainPresenter;
import com.firebase.client.Firebase;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class FirebaseManager {

    private static final String TAG = "FirebaseManager";

    private DatabaseReference rootReference;

    public FirebaseManager(Context context) {
        Firebase.setAndroidContext(context);

        rootReference = FirebaseDatabase.getInstance().getReference();
    }

    /**
     * Connect to FirebaseDatabase and push a message
     *
     * @param idCurrentUser Identifier of user who is logged in
     * @param idContactChatWith Contact identifier
     * @param data Message data
     *
     * @return True if the message is submitted successfully, false otherwise
     */
    public boolean sendMessage(int idCurrentUser, int idContactChatWith, Map<String, Object> data) {
        try {
            rootReference.child("chats/from/" + idCurrentUser + "/to/" + idContactChatWith).push().setValue(data);
            rootReference.child("chats/from/" + idContactChatWith + "/to/" + idCurrentUser).push().setValue(data);

            return true;
        } catch (Exception e) {
            Log.e(TAG, "Exception pushing message");
            return false;
        }
    }

    /**
     * Connect to FirebaseDatabase and retrieve all messages from a chat
     *
     * @param idCurrentUser Identifier of user who is logged in
     * @param contact Contact who you are chatting
     *
     * @return A list of messages
     */
    public FirebaseRecyclerAdapter<ChatMessage, ChatMessageViewHolder> getChatMessages(final int idCurrentUser, final Contact contact) {
        final FirebaseRecyclerAdapter<ChatMessage, ChatMessageViewHolder> adapter;

        try {
            adapter = new FirebaseRecyclerAdapter<ChatMessage, ChatMessageViewHolder>(
                    ChatMessage.class,
                    R.layout.chat_message_layout,
                    ChatMessageViewHolder.class,
                    rootReference.child("chats").child("from").child(idCurrentUser + "").child("to").child("" + contact.getId())) {

                @Override
                public ChatMessage getItem(int position) {
                    return super.getItem(position);
                }

                @Override
                protected void populateViewHolder(final ChatMessageViewHolder viewHolder, ChatMessage message, int position) {
                    try {
                        SimpleDateFormat formatDateTimeBBDD = new SimpleDateFormat("dd/MM/yyyy - HH:mm", Locale.getDefault());
                        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm", Locale.getDefault());

                        Date dateCurrentMessage = formatDateTimeBBDD.parse(message.getDatetime());

                        if (position == 0)
                            viewHolder.setDateMessageText(formatDate.format(dateCurrentMessage));
                        else {
                            Date dateLastMessage = formatDateTimeBBDD.parse(getItem(position - 1).getDatetime());
                            String stringDateCurrentMessage = formatDate.format(dateCurrentMessage);
                            String stringDateLastMessage = formatDate.format(dateLastMessage);

                            if (!stringDateLastMessage.equals(stringDateCurrentMessage))
                                viewHolder.setDateMessageText(stringDateCurrentMessage);
                            else {
                                viewHolder.hideDateMessageText();

                            }
                        }

                        viewHolder.setTimeMessageText(formatTime.format(dateCurrentMessage));
                    }
                    catch (Exception e) {
                        Log.e(TAG, "Exception parsing date: " + e.getMessage());
                    }

                    if (message.getUser_id() == idCurrentUser)
                        viewHolder.setStyleYourMessage();
                    else
                        viewHolder.setStyleContactMessage();

                    viewHolder.setContentMessage(message.getMessage());
                }
            };

            return adapter;
        } catch (Exception e) {
            Log.e(TAG, "Error retrieving data from server: " + e.toString());
            return null;
        }
    }

    /**
     * Connect to FirebaseDatabase and retrieve user info
     *
     * @param presenter Main controller in application
     * @param idUser Contact identifier
     */
    private void getInfoUser(final MainPresenter presenter, int idUser) {
        rootReference.child("users").child(idUser + "").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Contact contact = dataSnapshot.getValue(Contact.class);
                presenter.addContactToList(contact);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "The read failed: " + databaseError.getMessage());
            }
        });
    }

    /**
     * Connect to FirebaseDatabase and retrieve all contacts of the specified user
     *
     * @param idCurrentUser Identifier of user who is logged in
     * @param presenter Main controller in application
     */
    public void getContactList(int idCurrentUser, final MainPresenter presenter) {
        rootReference.child("contacts_list").child(idCurrentUser + "").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren())
                    getInfoUser(presenter, postSnapshot.getValue(Integer.class)); // Retrieve user with ID specified
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "The read failed: " + databaseError.getMessage());
            }
        });
    }
}
