package com.chat.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.chat.R;
import com.chat.interfaces.OnItemContactListListener;
import com.chat.model.Contact;
import com.chat.views.ContactViewHolder;
import java.util.ArrayList;

public class ContactRecyclerViewAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    private int idContactSelected = 2;
    private ArrayList<Contact> contactList;
    private final OnItemContactListListener listener;

    public ContactRecyclerViewAdapter(ArrayList<Contact> contactList, OnItemContactListListener listener) {
        this.contactList = contactList;
        this.listener = listener;
    }

    /**
     * Add a contact to arraylist and notify that one item has been added
     *
     * @param contact New contact
     */
    public void addContactToAdapter(Contact contact) {
        contactList.add(contact);
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_contact_layout, viewGroup, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        holder.textViewName.setText(contactList.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onContactClick(contactList.get(position));
                }
            }
        });

        if (contactList.get(position).getId() == idContactSelected)
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.bg_item_contact_list_selected));
        else
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.bg_item_contact_list));
    }

    /**
     * Update the field idContactSelected with the new identifier clicked
     *
     * @param idContact Identifier of the selected contact
     */
    public void updateContactIdSelected(int idContact) {
        idContactSelected = idContact;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
}
