package com.chat.interfaces;

import com.chat.model.Contact;

public interface OnItemContactListListener {
    void onContactClick(Contact o);
}
