package com.chat.presenters;

import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.chat.model.Contact;
import com.chat.managers.FirebaseManager;
import com.chat.R;
import com.chat.views.MainActivity;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainPresenter {

    private static final int idCurrentUser = 1;        //Define the ID of user that runs the application

    private int idContactChatWith;
    private MainActivity view;
    private FirebaseManager firebaseManager;

    @SuppressWarnings("ConstantConditions")
    public MainPresenter(MainActivity view) {
        this.view = view;

        firebaseManager = new FirebaseManager(view);

        //Depending on idCurrentUser value, set a default contact to show their chat
        if (idCurrentUser == 1) {
            idContactChatWith = 2;
            loadChatWithContact(new Contact(idContactChatWith, "Susana"));
        }
        else if (idCurrentUser == 2 || idCurrentUser == 3) {
            idContactChatWith = 1;
            loadChatWithContact(new Contact(idContactChatWith, "Antonio"));
        }
    }

    /**
     * Build a Hashmap with message info and send it to FirebaseManager
     *
     * @param message Chat message to be stored in FirebaseDatabase
     */
    public void sendMessage(String message){
        if (!message.trim().isEmpty()) {
            Map<String, Object> messageInfo = new HashMap<>();
            messageInfo.put("message", message);

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy - HH:mm", Locale.getDefault());
            String dateTime = df.format(Calendar.getInstance().getTime());

            messageInfo.put("datetime", dateTime);
            messageInfo.put("user_id", idCurrentUser);

            if (firebaseManager.sendMessage(idCurrentUser, idContactChatWith, messageInfo))
                view.clearInputMessage();
            else
                view.showErrorSendingMessage();
        }
    }

    /**
     * Retrieve all messages from a contact
     *
     * @param contact Contact about who you are doing the request
     */
    public void loadChatWithContact(Contact contact){
        idContactChatWith = contact.getId();

        view.getContactListAdapter().updateContactIdSelected(idContactChatWith);

        final FirebaseRecyclerAdapter adapter = firebaseManager.getChatMessages(idCurrentUser, contact);

        if (adapter == null)
            Toast.makeText(view, view.getResources().getString(R.string.error_retrieving_chat_messsages), Toast.LENGTH_SHORT).show();
        else {
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);

                    view.moveFocusToLastMessage();
                }
            });

            view.loadAdapterIntoRecyclerView(adapter);
        }
    }

    /**
     * Call to MainActivity reporting a new contact must be added to adapter
     *
     * @param contact New contact to be added
     */
    public void addContactToList(Contact contact){
        view.updateAdapterAddingContact(contact);
    }

    /**
     * Call FirebaseManager to proceed to request contacts
     */
    public void loadContactList(){
        firebaseManager.getContactList(idCurrentUser, this);
    }
}
