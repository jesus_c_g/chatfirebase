package com.chat.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chat.model.Contact;
import com.chat.adapters.ContactRecyclerViewAdapter;
import com.chat.presenters.MainPresenter;
import com.chat.R;
import com.chat.interfaces.OnItemContactListListener;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnItemContactListListener {

    private MainPresenter presenter;
    private EditText chatMessageInput;
    private ContactRecyclerViewAdapter contactListAdapter;
    private RecyclerView chatMessagesList;
    private RecyclerView contactsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chatMessageInput = (EditText) findViewById(R.id.chat_message_input);
        chatMessagesList = (RecyclerView) findViewById(R.id.recycler_view_chat_messages);
        contactsList = (RecyclerView) findViewById(R.id.recycler_view_contacts_list);

        chatMessagesList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        contactListAdapter = new ContactRecyclerViewAdapter(new ArrayList<Contact>(), MainActivity.this);
        presenter = new MainPresenter(this);

        initContactsRecyclerView();
        presenter.loadContactList();

        setSendMessageListener();
    }

    /**
     * When a new message is sent, auto scroll moves the focus to last message
     */
    public void moveFocusToLastMessage(){
        chatMessagesList.getLayoutManager().smoothScrollToPosition(chatMessagesList, null, chatMessagesList.getAdapter().getItemCount()-1);
    }

    /**
     * Setup and initialize contacts list
     */
    private void initContactsRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        contactsList.setHasFixedSize(true);

        contactsList.setAdapter(contactListAdapter);
        contactsList.setLayoutManager(layoutManager);
    }

    /**
     * Set OnClickListener in ImageView send_message_imageview
     */
    private void setSendMessageListener() {
        ImageView sendMessage = (ImageView) findViewById(R.id.send_message_imageview);

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.sendMessage(chatMessageInput.getText().toString());
            }
        });
    }

    /**
     * Add contact to adapter in recyclerview
     *
     * @param c New contact to add
     */
    public void updateAdapterAddingContact(Contact c) {
        contactListAdapter.addContactToAdapter(c);
    }

    /**
     * Show a Toast if an exception is raised saving message in FirebaseDatabase
     */
    public void showErrorSendingMessage() {
        Toast.makeText(this, getResources().getString(R.string.error_sending_message), android.widget.Toast.LENGTH_SHORT).show();
    }

    /**
     * Delete text from input message after sending to FirebaseDatabase
     */
    public void clearInputMessage() {
        chatMessageInput.setText("");
    }

    /**
     * Set adapter into ChatMessagesRecyclerView
     *
     * @param adapter Adapter with all messages
     */
    public void loadAdapterIntoRecyclerView(FirebaseRecyclerAdapter adapter) {
        chatMessagesList.setAdapter(adapter);
    }

    /**
     * Move scroll to last position to see last message added
     *
     * @param position Position to move
     */
    public void moveToLastMessage(int position) {
        chatMessagesList.scrollToPosition(position);
    }

    /**
     * Retrieve last position with item visible
     *
     * @return Index of last item visible
     */
    public int getPositionLastItemVisible() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) chatMessagesList.getLayoutManager());

        if (layoutManager != null)
            return layoutManager.findLastCompletelyVisibleItemPosition();
        else
            return -1;
    }

    /**
     * Getter for attribute contactListAdapter
     *
     * @return contactListAdapter
     */
    public ContactRecyclerViewAdapter getContactListAdapter(){
        return contactListAdapter;
    }

    @Override
    public void onContactClick(Contact contact) {
        presenter.loadChatWithContact(contact);
    }

}
