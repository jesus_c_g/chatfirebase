package com.chat.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.chat.R;

public class ContactViewHolder extends RecyclerView.ViewHolder {

    public final View itemView;
    public final TextView textViewName;

    public ContactViewHolder(View itemView) {
        super(itemView);

        this.itemView = itemView;

        textViewName = (TextView) itemView.findViewById(R.id.text_view_name_contact);
    }
}
