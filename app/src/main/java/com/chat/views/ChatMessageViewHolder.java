package com.chat.views;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chat.R;

public  class ChatMessageViewHolder extends RecyclerView.ViewHolder {

    private final TextView contentMessage, dateMessage, timeMesssage;

    public ChatMessageViewHolder(View v) {
        super(v);

        contentMessage = (TextView) v.findViewById(R.id.content_message_text);
        dateMessage = (TextView) v.findViewById(R.id.date_text);
        timeMesssage = (TextView) v.findViewById(R.id.time_text);
    }

    /**
     * Set general params to LayoutParams shared by different views
     */
    private void setGeneralLayoutParams(){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 4, 0 , 4);

        contentMessage.setLayoutParams(layoutParams);
        timeMesssage.setLayoutParams(layoutParams);
    }

    /**
     * Set date text of a message
     *
     * @param date Date to show
     */
    public void setDateMessageText(String date){
        dateMessage.setVisibility(View.VISIBLE);
        dateMessage.setText(date);
    }

    /**
     * Set time text of a message
     *
     * @param time Time to show
     */
    public void setTimeMessageText(String time){
        timeMesssage.setText(time);
    }

    /**
     * Show message
     *
     * @param contentMessage Message to show
     */
    public void setContentMessage(String contentMessage){
        this.contentMessage.setText(contentMessage);
    }

    /**
     * Hide date text when different messages are written at the same day
     */
    public void hideDateMessageText(){
        dateMessage.setVisibility(View.GONE);
    }

    /**
     * Define the design of a message written by a contact
     */
    public void setStyleYourMessage(){
        setGeneralLayoutParams();

       ((LinearLayout.LayoutParams) contentMessage.getLayoutParams()).gravity = Gravity.END;

        contentMessage.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.rounded_bg_chat_your_message));
        contentMessage.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.your_messages_chat));
    }

    /**
     * Define the design of a message written by the user
     */
    public void setStyleContactMessage(){
        setGeneralLayoutParams();

        ((LinearLayout.LayoutParams) contentMessage.getLayoutParams()).gravity = Gravity.START;

        contentMessage.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.other_messages_chat));
        contentMessage.setBackground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.rounded_bg_chat_message));
    }
}